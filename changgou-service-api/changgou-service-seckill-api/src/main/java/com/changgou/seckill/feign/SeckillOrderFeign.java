package com.changgou.seckill.feign;

import com.changgou.common.response.Result;
import com.changgou.seckill.bean.SeckillOrder;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SeckillOrderFeign
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@FeignClient(name = "seckill-service", contextId = "seckillOrderFeign")
@RequestMapping("/seckillOrder")
public interface SeckillOrderFeign {
    /**
     * 获取随机码
     */
    @GetMapping("/random")
    Result<String> random();

    /**
     * 下单
     */
    @PostMapping("/add")
    Result<String> add(@RequestParam("time") String time,
                       @RequestParam("id") Long id,
                       @RequestParam("random") String random);

    /**
     * 下单
     */
    @PostMapping("/create")
    Result<Boolean> create(@RequestParam("time") String time, @RequestParam("id") Long id);

    /**
     * 新增SeckillOrder数据
     *
     * @param seckillOrder
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody SeckillOrder seckillOrder);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改SeckillOrder数据
     *
     * @param seckillOrder
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody SeckillOrder seckillOrder, @PathVariable Long id);

    /**
     * 根据ID查询SeckillOrder数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<SeckillOrder> findById(@PathVariable Long id);

    /**
     * 查询SeckillOrder全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<SeckillOrder>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param seckillOrder
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<SeckillOrder>> findList(@RequestBody(required = false) SeckillOrder seckillOrder);

    /**
     * SeckillOrder分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<SeckillOrder>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * SeckillOrder分页条件搜索实现
     *
     * @param seckillOrder
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<SeckillOrder>> findPage(@RequestBody(required = false) SeckillOrder seckillOrder, @PathVariable int page, @PathVariable int size);

}