package com.changgou.seckill.feign;

import com.changgou.common.response.Result;
import com.changgou.seckill.bean.SeckillGoods;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SeckillGoodsFeign
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@FeignClient(name = "seckill-service", contextId = "seckillGoodsFeign")
@RequestMapping("/seckillGoods")
public interface SeckillGoodsFeign {
    /**
     * 新增SeckillGoods数据
     *
     * @param seckillGoods
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody SeckillGoods seckillGoods);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改SeckillGoods数据
     *
     * @param seckillGoods
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody SeckillGoods seckillGoods, @PathVariable Long id);

    /**
     * 根据ID查询SeckillGoods数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<SeckillGoods> findById(@PathVariable Long id);

    /**
     * 查询SeckillGoods全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<SeckillGoods>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param seckillGoods
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<SeckillGoods>> findList(@RequestBody(required = false) SeckillGoods seckillGoods);

    /**
     * 获取当前的时间基准的5个时间段
     *
     * @return
     */
    @GetMapping("/menus")
    Result<List<String>> menus();

    @GetMapping(value = "/list")
    Result<List<SeckillGoods>> list(@RequestParam("time") String time);

    @PostMapping(value = "/one")
    Result<SeckillGoods> one(@RequestParam("time") String time, @RequestParam("id") Long id);

    /**
     * SeckillGoods分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<SeckillGoods>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * SeckillGoods分页条件搜索实现
     *
     * @param seckillGoods
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<SeckillGoods>> findPage(@RequestBody(required = false) SeckillGoods seckillGoods, @PathVariable int page, @PathVariable int size);

}