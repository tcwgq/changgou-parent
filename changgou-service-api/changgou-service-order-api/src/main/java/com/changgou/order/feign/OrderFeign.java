package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.Order;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "orderFeign")
@RequestMapping("/order")
public interface OrderFeign {
    /**
     * 下单
     *
     * @param order
     * @return
     */
    @PostMapping("/create")
    Result<Order> create(@RequestBody Order order);

    /**
     * 新增Order数据
     *
     * @param order
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Order order);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改Order数据
     *
     * @param order
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Order order, @PathVariable String id);

    /**
     * 根据ID查询Order数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Order> findById(@PathVariable String id);

    /**
     * 查询Order全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Order>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param order
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Order>> findList(@RequestBody(required = false) Order order);

    /**
     * Order分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Order>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Order分页条件搜索实现
     *
     * @param order
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Order>> findPage(@RequestBody(required = false) Order order, @PathVariable int page, @PathVariable int size);

}