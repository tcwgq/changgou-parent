package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.TaskHis;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TaskHisFeign
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@FeignClient(name = "order-service", contextId = "taskHisFeign")
@RequestMapping("/taskHis")
public interface TaskHisFeign {
    /**
     * 新增TaskHis数据
     *
     * @param taskHis
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody TaskHis taskHis);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改TaskHis数据
     *
     * @param taskHis
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody TaskHis taskHis, @PathVariable Long id);

    /**
     * 根据ID查询TaskHis数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<TaskHis> findById(@PathVariable Long id);

    /**
     * 查询TaskHis全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<TaskHis>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param taskHis
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<TaskHis>> findList(@RequestBody(required = false) TaskHis taskHis);

    /**
     * TaskHis分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<TaskHis>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * TaskHis分页条件搜索实现
     *
     * @param taskHis
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<TaskHis>> findPage(@RequestBody(required = false) TaskHis taskHis, @PathVariable int page, @PathVariable int size);

}