package com.changgou.order.bean;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * TaskBean
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@Data
@Table(name = "tb_task")
public class Task implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;// 任务id

    @Column(name = "create_time")
    private Date createTime;//

    @Column(name = "update_time")
    private Date updateTime;//

    @Column(name = "delete_time")
    private Date deleteTime;//

    @Column(name = "task_type")
    private String taskType;// 任务类型

    @Column(name = "mq_exchange")
    private String mqExchange;// 交换机名称

    @Column(name = "mq_routingkey")
    private String mqRoutingkey;// routingkey

    @Column(name = "request_body")
    private String requestBody;// 任务请求的内容

    @Column(name = "status")
    private String status;// 任务状态

    @Column(name = "errormsg")
    private String errormsg;// 任务错误信息

}
