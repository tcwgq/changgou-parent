package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.OrderConfig;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderConfigFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "orderConfigFeign")
@RequestMapping("/orderConfig")
public interface OrderConfigFeign {
    /**
     * 新增OrderConfig数据
     *
     * @param orderConfig
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody OrderConfig orderConfig);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Integer id);

    /**
     * 修改OrderConfig数据
     *
     * @param orderConfig
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody OrderConfig orderConfig, @PathVariable Integer id);

    /**
     * 根据ID查询OrderConfig数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<OrderConfig> findById(@PathVariable Integer id);

    /**
     * 查询OrderConfig全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<OrderConfig>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param orderConfig
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<OrderConfig>> findList(@RequestBody(required = false) OrderConfig orderConfig);

    /**
     * OrderConfig分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderConfig>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * OrderConfig分页条件搜索实现
     *
     * @param orderConfig
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderConfig>> findPage(@RequestBody(required = false) OrderConfig orderConfig, @PathVariable int page, @PathVariable int size);

}