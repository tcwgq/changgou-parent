package com.changgou.search.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @since 2022/6/8 16:01
 */
@Data
public class SkuInfoVo implements Serializable {
    //@id 表示文档的唯一标识
    private Long id;

    // SKU名称
    private String name;

    // 商品价格，单位为：元
    private Long price;

    // 库存数量
    private Integer num;

    // 商品图片
    private String image;

    // 商品状态，1-正常，2-下架，3-删除
    private String status;

    // 创建时间
    private Date createTime;

    // 更新时间
    private Date updateTime;

    // 是否默认
    private String isDefault;

    // SPUID
    private Long spuId;

    // 类目ID
    private Long categoryId;

    // 类目名称
    private String categoryName;

    // 品牌名称
    private String brandName;

    // 规格
    private String spec;

    // 动态的域的添加和变化
    private Map<String, Object> specMap;

}
