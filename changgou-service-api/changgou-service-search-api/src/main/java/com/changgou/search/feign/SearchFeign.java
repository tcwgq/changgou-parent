package com.changgou.search.feign;

import com.changgou.common.response.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 描述
 *
 * @since 2022/6/8 16:01
 */
@FeignClient(name = "search-service", contextId = "searchFeign")
@RequestMapping("/search")
public interface SearchFeign {
    @GetMapping
    Result<Map<String, Object>> search(@RequestParam(required = false) Map<String, String> searchMap);

}
