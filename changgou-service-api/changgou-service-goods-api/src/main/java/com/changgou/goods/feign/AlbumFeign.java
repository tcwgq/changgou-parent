package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Album;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * AlbumFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "albumFeign")
@RequestMapping("/album")
public interface AlbumFeign {
    /**
     * 新增Album数据
     *
     * @param album
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Album album);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Album数据
     *
     * @param album
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Album album, @PathVariable Long id);

    /**
     * 根据ID查询Album数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Album> findById(@PathVariable Long id);

    /**
     * 查询Album全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Album>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param album
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Album>> findList(@RequestBody(required = false) Album album);

    /**
     * Album分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Album>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Album分页条件搜索实现
     *
     * @param album
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Album>> findPage(@RequestBody(required = false) Album album, @PathVariable int page, @PathVariable int size);

}