package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * BrandFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "brandFeign")
@RequestMapping("/brand")
public interface BrandFeign {
    /**
     * 新增Brand数据
     *
     * @param brand
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Brand brand);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Brand数据
     *
     * @param brand
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Brand brand, @PathVariable Long id);

    /**
     * 根据ID查询Brand数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Brand> findById(@PathVariable Long id);

    /**
     * 查询Brand全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Brand>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param brand
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Brand>> findList(@RequestBody(required = false) Brand brand);

    /**
     * Brand分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Brand>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Brand分页条件搜索实现
     *
     * @param brand
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Brand>> findPage(@RequestBody(required = false) Brand brand, @PathVariable int page, @PathVariable int size);

}