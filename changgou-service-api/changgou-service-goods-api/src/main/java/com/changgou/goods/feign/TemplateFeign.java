package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Template;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TemplateFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "templateFeign")
@RequestMapping("/template")
public interface TemplateFeign {
    /**
     * 新增Template数据
     *
     * @param template
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Template template);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Template数据
     *
     * @param template
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Template template, @PathVariable Long id);

    /**
     * 根据ID查询Template数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Template> findById(@PathVariable Long id);

    /**
     * 查询Template全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Template>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param template
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Template>> findList(@RequestBody(required = false) Template template);

    /**
     * Template分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Template>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Template分页条件搜索实现
     *
     * @param template
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Template>> findPage(@RequestBody(required = false) Template template, @PathVariable int page, @PathVariable int size);

}