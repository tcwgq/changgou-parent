package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Category;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * CategoryFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "categoryFeign")
@RequestMapping("/category")
public interface CategoryFeign {
    /**
     * 新增Category数据
     *
     * @param category
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Category category);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Category数据
     *
     * @param category
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Category category, @PathVariable Long id);

    /**
     * 根据ID查询Category数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Category> findById(@PathVariable Long id);

    /**
     * 查询Category全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Category>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param category
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Category>> findList(@RequestBody(required = false) Category category);

    /**
     * Category分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Category>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Category分页条件搜索实现
     *
     * @param category
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Category>> findPage(@RequestBody(required = false) Category category, @PathVariable int page, @PathVariable int size);

}