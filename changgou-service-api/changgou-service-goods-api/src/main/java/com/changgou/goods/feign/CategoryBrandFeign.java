package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.CategoryBrand;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * CategoryBrandFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "categoryBrandFeign")
@RequestMapping("/categoryBrand")
public interface CategoryBrandFeign {
    /**
     * 新增CategoryBrand数据
     *
     * @param categoryBrand
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody CategoryBrand categoryBrand);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改CategoryBrand数据
     *
     * @param categoryBrand
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody CategoryBrand categoryBrand, @PathVariable Long id);

    /**
     * 根据ID查询CategoryBrand数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<CategoryBrand> findById(@PathVariable Long id);

    /**
     * 查询CategoryBrand全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<CategoryBrand>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param categoryBrand
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<CategoryBrand>> findList(@RequestBody(required = false) CategoryBrand categoryBrand);

    /**
     * CategoryBrand分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<CategoryBrand>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * CategoryBrand分页条件搜索实现
     *
     * @param categoryBrand
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<CategoryBrand>> findPage(@RequestBody(required = false) CategoryBrand categoryBrand, @PathVariable int page, @PathVariable int size);

}