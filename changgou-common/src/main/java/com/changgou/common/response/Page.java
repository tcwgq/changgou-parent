package com.changgou.common.response;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {
    // 全局偏移量
    public int offSize = 2;
    // 页数（第几页）
    private long currentPage;
    private int size;
    // 从数据库查询的总记录数
    private long total;
    private long start;
    private int last;
    private int lPage;
    private int rPage;
    private List<T> list;

    public Page() {
        super();
    }

    public Page(long total, int currentPage, int pageSize) {
        initPage(total, currentPage, pageSize);
    }

    public Page(long total, int currentPage, int pageSize, int offSize) {
        this.offSize = offSize;
        initPage(total, currentPage, pageSize);
    }

    public void setCurrentPage(long currentPage, long total, long pageSize) {
        // 如果整除表示正好分N页，如果不能整除在N页的基础上+1页
        int totalPages = (int) (total % pageSize == 0 ? total / pageSize : (total / pageSize) + 1);

        // 总页数
        this.last = totalPages;

        // 判断当前页是否越界,如果越界，我们就查最后一页
        if (currentPage > totalPages) {
            this.currentPage = totalPages;
        } else {
            this.currentPage = currentPage;
        }

        // 计算start
        this.start = (this.currentPage - 1) * pageSize;
    }

    public void initPage(long total, int currentPage, int pageSize) {
        // 总记录数
        this.total = total;
        // 每页显示多少条
        this.size = pageSize;

        // 计算当前页和数据库查询起始值以及总页数
        setCurrentPage(currentPage, total, pageSize);

        // 分页计算
        int leftCount = this.offSize;                 // 需要向上一页执行多少次
        int rightCount = this.offSize;

        // 2点判断
        this.lPage = currentPage - leftCount;         // 正常情况下的起点
        this.rPage = currentPage + rightCount;        // 正常情况下的终点

        // 页差=总页数和结束页的差
        int topDiv = this.last - rPage;               // 判断是否大于最大页数

        /*
         * 起点页
         * 1、页差<0  起点页=起点页+页差值
         * 2、页差>=0 起点和终点判断
         */
        this.lPage = topDiv < 0 ? this.lPage + topDiv : this.lPage;

        /*
         * 结束页
         * 1、起点页<=0   结束页=|起点页|+1
         * 2、起点页>0    结束页
         */
        this.rPage = this.lPage <= 0 ? this.rPage + (this.lPage * -1) + 1 : this.rPage;

        /*
         * 当起点页<=0  让起点页为第一页
         * 否则不管
         */
        this.lPage = this.lPage <= 0 ? 1 : this.lPage;

        /*
         * 如果结束页>总页数   结束页=总页数
         * 否则不管
         */
        this.rPage = Math.min(this.rPage, last);
    }

    public long getUpper() {
        return currentPage > 1 ? currentPage - 1 : currentPage;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public long getNext() {
        return currentPage < last ? currentPage + 1 : last;
    }

    public long getTotal() {
        return total;
    }

    public long getSize() {
        return size;
    }

    public long getStart() {
        return start;
    }

    public long getLast() {
        return last;
    }

    public long getlPage() {
        return lPage;
    }

    public long getrPage() {
        return rPage;
    }

    public List<T> getList() {
        return list;
    }

}
