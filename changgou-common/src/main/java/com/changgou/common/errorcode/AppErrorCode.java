package com.changgou.common.errorcode;

/**
 * 业务系统错误码为10位整数
 * 前5位表示业务系统代号，每个业务系统代号唯一（考虑到业务系统有可能发展到几千个）
 * 第6位表示表示错误的类别，含义如下：
 * 1 系统级错误，如数据库宕机 Redis宕机等
 * 2 应用级错误，如参数错误，数据格式校验不通过
 * 3 业务级错误，如员工信息不存在，用户已被禁用
 * 4 依赖服务错误，如passport服务异常
 * 5 交互级错误，如库存不足，订单重复提交
 * 后4位表示业务系统里的错误代码，由业务系统自行规划
 * 可以抽取2位表示具体模块的编号，剩余2位表示具体模块的错误码
 * <p>
 * 如passport系统的代号为10000
 * 系统代号 错误类型 具体模块 具体模块错误码
 * 10000    1       10        99
 * 注意：int类型最大值为2147483647
 *
 * @author tcwgq
 * @time 2020/9/14 22:16
 */
public class AppErrorCode extends BaseErrorCode {
    private static final int APP_ID = 10000;

    public static ErrorCode USER_NOT_ALLOW_LOGIN = new AppErrorCode(112, 12, "用户不允许登录");

    public AppErrorCode(Integer code, String msg) {
        super(code, msg);
    }

    public AppErrorCode(Integer tb, Integer no, String msg) {
        super(APP_ID, tb, no, msg);
    }

    public AppErrorCode(Integer type, Integer businessId, Integer no, String msg) {
        super(APP_ID, type, businessId, no, msg);
    }

    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Long.MAX_VALUE);
        System.out.println(USER_NOT_ALLOW_LOGIN.toString());
    }
}
