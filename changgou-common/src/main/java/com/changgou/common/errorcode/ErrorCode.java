package com.changgou.common.errorcode;

import java.io.Serializable;

/**
 * @author tcwgq
 * @time 2020/9/14 21:32
 */
public interface ErrorCode extends Serializable {
    Integer getCode();

    String getMessage();

}
