package com.changgou.common.exception;

import com.changgou.common.errorcode.BaseErrorCode;
import com.changgou.common.errorcode.ErrorCode;

/**
 * @author tcwgq
 * @since 2022/6/25 11:29
 */
public class ChanggouException extends BaseErrorCode {

    public ChanggouException(ErrorCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }

    public ChanggouException(Integer code, String message) {
        super(code, message);
    }

    public ChanggouException(Integer appId, Integer tb, Integer no, String message) {
        super(appId, tb, no, message);
    }

    public ChanggouException(Integer appId, Integer type, Integer businessId, Integer no, String message) {
        super(appId, type, businessId, no, message);
    }

}
