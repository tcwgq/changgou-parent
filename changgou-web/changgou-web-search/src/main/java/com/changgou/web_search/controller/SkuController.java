package com.changgou.web_search.controller;

import com.changgou.common.response.Page;
import com.changgou.common.response.Result;
import com.changgou.search.bean.SkuInfoVo;
import com.changgou.search.feign.SearchFeign;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/6/10 17:19
 */
@Controller
@RequestMapping("/search")
public class SkuController {
    @Autowired
    private SearchFeign searchFeign;

    @GetMapping("/list")
    public String search(@RequestParam Map<String, String> searchMap, Model model) throws Exception {
        Result<Map<String, Object>> result = searchFeign.search(searchMap);
        model.addAttribute("request", searchMap);
        model.addAttribute("result", result);
        String[] urls = url(searchMap);
        model.addAttribute("url", urls[0]);
        model.addAttribute("sortUrl", urls[1]);

        String pageNo = "1";
        String pageSize = "10";
        if (searchMap != null && StringUtils.isNotEmpty(searchMap.get("pageNo"))) {
            pageNo = searchMap.get("pageNo");
        }
        if (searchMap != null && StringUtils.isNotEmpty(searchMap.get("pageSize"))) {
            pageSize = searchMap.get("pageSize");
        }
        int i = Integer.parseInt(pageSize);
        if (i > 5000) {
            i = 5000;
        }
        // 创建一个分页的对象  可以获取当前页 和总个记录数和显示的页码(以当前页为中心的5个页码)
        Map<String, Object> data = result.getData();
        Page<SkuInfoVo> pageInfo = new Page<>(
                Long.parseLong(data.get("total").toString()),
                Integer.parseInt(pageNo),
                i
        );
        model.addAttribute("pageInfo", pageInfo);

        return "search";
    }

    private String[] url(Map<String, String> searchMap) {
        StringBuilder url = new StringBuilder("/search/list");
        StringBuilder sortUrl = new StringBuilder("/search/list");
        if (searchMap != null && searchMap.size() > 0) {
            url.append("?");
            sortUrl.append("?");
            for (Map.Entry<String, String> entry : searchMap.entrySet()) {
                String key = entry.getKey();// keywords / brand  / category
                String value = entry.getValue();// 华为  / 华为  / 笔记本
                if (key.equals("pageNo") || key.equals("pageSize")) {
                    continue;
                }
                url.append(key).append("=").append(value).append("&");
                if (key.equals("sortField") || key.equals("sortRule")) {
                    continue;
                }
                sortUrl.append(key).append("=").append(value).append("&");
            }

            // 去掉多余的&
            if (url.lastIndexOf("&") != -1) {
                url = new StringBuilder(url.substring(0, url.lastIndexOf("&")));
            }
            if (sortUrl.lastIndexOf("&") != -1) {
                sortUrl = new StringBuilder(sortUrl.substring(0, sortUrl.lastIndexOf("&")));
            }

        }
        return new String[]{url.toString(), sortUrl.toString()};
    }

}
