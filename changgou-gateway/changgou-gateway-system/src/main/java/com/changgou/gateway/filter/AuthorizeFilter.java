package com.changgou.gateway.filter;

import com.changgou.common.utils.JwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * jwt原始令牌拦截校验
 *
 * @author tcwgq
 * @since 2022/6/13 10:00
 */
public class AuthorizeFilter implements GlobalFilter, Ordered {
    private final String TOKEN_NAME = "access_token";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String token = getToken(request);
        if (StringUtils.isEmpty(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        try {
            JwtUtil.parseJWT(token);
        } catch (Exception e) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        // 将token放到请求头中，传递到下个微服务
        request.mutate().header(TOKEN_NAME, token);
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private String getToken(ServerHttpRequest request) {
        String first = request.getHeaders().getFirst(TOKEN_NAME);
        if (StringUtils.isNotEmpty(first)) {
            return first;
        }

        String second = request.getQueryParams().getFirst(TOKEN_NAME);
        if (StringUtils.isNotEmpty(second)) {
            return second;
        }

        HttpCookie third = request.getCookies().getFirst(TOKEN_NAME);
        if (third != null) {
            String value = third.getValue();
            if (StringUtils.isNotEmpty(value)) {
                return value;
            }
        }
        return "";
    }

}
