package com.changgou.seckill.service.impl.redis;

import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.constants.SystemConstants;
import com.changgou.seckill.service.SeckillOrderRedisService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * SeckillOrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@Service
public class SeckillOrderRedisServiceImpl implements SeckillOrderRedisService, InitializingBean {
    @Resource(name = "orderRedisTemplate")
    private RedisTemplate<String, SeckillOrder> redisTemplate;

    @Override
    public void afterPropertiesSet() {

    }

    @Override
    public void put(String username, SeckillOrder seckillOrder) {
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_ORDER_KEY).put(username, seckillOrder);
    }

    @Override
    public SeckillOrder get(String username) {
        BoundHashOperations<String, String, SeckillOrder> boundHashOperations = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_ORDER_KEY);
        return boundHashOperations.get(username);
    }

    @Override
    public void delete(String username) {
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_ORDER_KEY).delete(username);
    }

}
