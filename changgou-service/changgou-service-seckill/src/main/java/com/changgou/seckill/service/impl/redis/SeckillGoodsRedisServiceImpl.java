package com.changgou.seckill.service.impl.redis;

import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.constants.SystemConstants;
import com.changgou.seckill.service.SeckillGoodsCountRedisService;
import com.changgou.seckill.service.SeckillGoodsRedisService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * SeckillGoodsServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@Service
public class SeckillGoodsRedisServiceImpl implements SeckillGoodsRedisService, InitializingBean {
    @Resource(name = "goodsRedisTemplate")
    private RedisTemplate<String, SeckillGoods> redisTemplate;

    @Resource
    private SeckillGoodsCountRedisService seckillGoodsCountRedisService;

    @Override
    public void afterPropertiesSet() {

    }

    @Override
    public List<SeckillGoods> list(String time) {
        BoundHashOperations<String, Long, SeckillGoods> hashOperations = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX + time);
        return hashOperations.values();
    }

    @Override
    public SeckillGoods get(String time, Long id) {
        BoundHashOperations<String, Long, SeckillGoods> hashOperations = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX + time);
        return hashOperations.get(id);
    }

    @Override
    public void put(String time, SeckillGoods seckillGoods) {
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX + time).put(seckillGoods.getId(), seckillGoods);
    }

    @Override
    public void delete(String time, Long id) {
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX + time).delete(id);
    }

    @Override
    public void clearGoods(String time, Long id) {
        delete(time, id);
        seckillGoodsCountRedisService.deleteCount(id);
    }

}
