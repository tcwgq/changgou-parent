package com.changgou.seckill.listener;

import com.changgou.seckill.bean.SeckillStatus;
import com.changgou.seckill.config.RabbitMQOrderAutoCloseConfig;
import com.changgou.seckill.service.SeckillOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * TODO 消息监听时，无法获取请求头中的授权token
 */
@Slf4j
@Component
public class OrderAutoCloseListener {
    @Resource
    private SeckillOrderService orderService;

    /**
     * TODO 消息幂等处理
     *
     * @param message 消息
     */
    @RabbitListener(queues = RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_DEAD_QUEUE)
    public void receiveMessage(SeckillStatus message) {
        log.info("message: {}", message);
        orderService.close(message);
    }

}