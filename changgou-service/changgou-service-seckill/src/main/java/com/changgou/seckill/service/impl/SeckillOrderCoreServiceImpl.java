package com.changgou.seckill.service.impl;

import com.alibaba.fastjson2.JSON;
import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.utils.IdWorker;
import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.bean.SeckillStatus;
import com.changgou.seckill.config.RabbitMQOrderAutoCloseConfig;
import com.changgou.seckill.config.RabbitMQOrderConfig;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.listener.CustomMessageSender;
import com.changgou.seckill.service.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * SeckillOrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@Service
public class SeckillOrderCoreServiceImpl implements SeckillOrderCoreService {
    @Resource
    private IdWorker idWorker;
    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    private CustomMessageSender customMessageSender;
    @Resource
    private SeckillGoodsMapper seckillGoodsMapper;
    @Resource
    private SeckillGoodsRedisService seckillGoodsRedisService;
    @Resource
    private SeckillOrderRedisService seckillOrderRedisService;
    @Resource
    private SeckillOrderQueueService seckillOrderQueueService;
    @Resource
    private SeckillGoodsCountRedisService seckillGoodsCountRedisService;

    @Override
    public boolean create(Long id, String time, String username) {
        Integer count = seckillGoodsCountRedisService.get(id);
        // 没有库存
        if (count == null || count <= 0) {
            // 清除排队信息
            seckillOrderQueueService.clearUserQueue(username);
            // 清除商品信息，清除库存信息
            seckillGoodsRedisService.clearGoods(time, id);
            throw new ChanggouException(ChanggouErrorCode.GOOD_LACK);
        }
        // 1.根据商品的ID获取秒杀商品的数据
        SeckillGoods seckillGoods = seckillGoodsRedisService.get(time, id);
        // 2.判断是否有库存
        if (seckillGoods == null || seckillGoods.getStockCount() <= 0) {
            // 清除商品信息，清除库存信息
            seckillGoodsRedisService.clearGoods(time, id);
            throw new ChanggouException(ChanggouErrorCode.GOOD_SOLD_OUT);
        }

        // 3.创建一个预订单
        SeckillOrder seckillOrder = new SeckillOrder();
        seckillOrder.setId(idWorker.nextId());// 订单的ID
        seckillOrder.setSeckillId(id);// 秒杀商品ID
        seckillOrder.setMoney(seckillGoods.getCostPrice());// 金额
        seckillOrder.setUserId(username);// 登录的用户名
        seckillOrder.setCreateTime(new Date());// 创建时间
        seckillOrder.setStatus("0");// 未支付

        // 4. 递减库存，商品是最后一个则从redis删除
        Long decrease = seckillGoodsCountRedisService.decrease(id);
        seckillGoods.setStockCount(Math.toIntExact(decrease));
        if (decrease <= 0) {
            // 清除商品信息，清除库存信息
            seckillGoodsRedisService.clearGoods(time, id);
        } else {
            // 同步到redis
            seckillGoodsRedisService.put(time, seckillGoods);
        }

        // 5.入库
        customMessageSender.sendMessage(RabbitMQOrderConfig.ORDER_PAY_NOTIFY_EXCHANGE, "", JSON.toJSONString(seckillOrder));

        // 6.更新队列状态
        SeckillStatus seckillStatus = new SeckillStatus();
        seckillStatus.setUsername(username);
        seckillStatus.setGoodsId(id);
        seckillStatus.setStatus(2);
        seckillStatus.setOrderId(seckillOrder.getId());
        seckillStatus.setMoney(seckillOrder.getMoney());
        seckillStatus.setCreateTime(seckillOrder.getCreateTime());

        // 发送自动过期自动关单消息
        rabbitTemplate.convertAndSend(RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_EXCHANGE, "XA", seckillStatus);

        return true;
    }

    @Async
    @Override
    public void createAsync() {
        SeckillStatus seckillStatus = seckillOrderQueueService.rightPop();
        if (seckillStatus != null) {
            Long id = seckillStatus.getGoodsId();// 秒杀商品的ID
            String time = seckillStatus.getTime();
            String username = seckillStatus.getUsername();

            Integer count = seckillGoodsCountRedisService.get(id);
            // 没有库存
            if (count == null || count <= 0) {
                // 清除排队信息
                seckillOrderQueueService.clearUserQueue(username);
                //  清除商品信息，清除库存信息
                seckillGoodsRedisService.clearGoods(time, id);
                throw new ChanggouException(ChanggouErrorCode.GOOD_LACK);
            }
            // 1.根据商品的ID获取秒杀商品的数据
            SeckillGoods seckillGoods = seckillGoodsRedisService.get(time, id);
            // 2.判断是否有库存
            if (seckillGoods == null || seckillGoods.getStockCount() <= 0) {
                // 清除商品信息，清除库存信息
                seckillGoodsRedisService.clearGoods(time, id);
                throw new ChanggouException(ChanggouErrorCode.GOOD_SOLD_OUT);
            }

            // 3.创建一个预订单
            SeckillOrder seckillOrder = new SeckillOrder();
            seckillOrder.setId(idWorker.nextId());// 订单的ID
            seckillOrder.setSeckillId(id);// 秒杀商品ID
            seckillOrder.setMoney(seckillGoods.getCostPrice());// 金额
            seckillOrder.setUserId(username);// 登录的用户名
            seckillOrder.setCreateTime(new Date());// 创建时间
            seckillOrder.setStatus("0");// 未支付

            // TODO 入库，使用消息解耦
            seckillOrderRedisService.put(username, seckillOrder);

            // 4. 递减库存，商品有可能是最后一个，是最后一个，从redis删除，并且将数据同步到mysql
            // seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
            // 通过redis扣减库存
            Long decrease = seckillGoodsCountRedisService.decrease(id);
            seckillGoods.setStockCount(Math.toIntExact(decrease));
            if (decrease <= 0) {
                // TODO MQ方式同步库存到数据库
                seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
                // 清除商品信息，清除库存信息
                seckillGoodsRedisService.clearGoods(time, id);
            } else {
                // 同步到redis
                seckillGoodsRedisService.put(time, seckillGoods);
            }

            // 5.更新队列状态
            seckillStatus.setUsername(username);
            seckillStatus.setGoodsId(id);
            seckillStatus.setStatus(2);
            seckillStatus.setOrderId(seckillOrder.getId());
            seckillStatus.setMoney(seckillOrder.getMoney());
            seckillStatus.setCreateTime(seckillOrder.getCreateTime());
            seckillOrderQueueService.put(username, seckillStatus);

            // 发送自动过期自动关单消息，TODO 异步发送
            rabbitTemplate.convertAndSend(RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_EXCHANGE, "XA", seckillStatus);
        }
    }

}
