package com.changgou.seckill.service;

import com.changgou.seckill.bean.SeckillGoods;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * SeckillGoodsService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillGoodsService {
    /**
     * 新增SeckillGoods
     *
     * @param seckillGoods
     */
    void add(SeckillGoods seckillGoods);

    /**
     * 删除SeckillGoods
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 修改SeckillGoods数据
     *
     * @param seckillGoods
     */
    void update(SeckillGoods seckillGoods);

    /**
     * 根据ID查询SeckillGoods
     *
     * @param id
     * @return
     */
    SeckillGoods findById(Long id);

    /**
     * 查询所有SeckillGoods
     *
     * @return
     */
    List<SeckillGoods> findAll();

    /**
     * SeckillGoods多条件搜索方法
     *
     * @param seckillGoods
     * @return
     */
    List<SeckillGoods> findList(SeckillGoods seckillGoods);

    List<SeckillGoods> list(String time);

    SeckillGoods one(String time, Long id);

    /**
     * SeckillGoods分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<SeckillGoods> findPage(int page, int size);

    /**
     * SeckillGoods多条件分页查询
     *
     * @param seckillGoods
     * @param page
     * @param size
     * @return
     */
    PageInfo<SeckillGoods> findPage(SeckillGoods seckillGoods, int page, int size);

}
