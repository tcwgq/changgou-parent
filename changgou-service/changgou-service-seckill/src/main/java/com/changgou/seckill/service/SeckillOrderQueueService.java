package com.changgou.seckill.service;

import com.changgou.seckill.bean.SeckillStatus;

/**
 * SeckillOrderQueueService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillOrderQueueService {

    Long increment(String username);

    Boolean deleteCount(String username);

    void leftPush(SeckillStatus seckillStatus);

    SeckillStatus rightPop();

    void put(String username, SeckillStatus seckillStatus);

    void remove(String username);

    SeckillStatus status(String username);

    void clearUserQueue(String username);
}
