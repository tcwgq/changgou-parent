package com.changgou.seckill.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.changgou.seckill.config.RabbitMQWeChatPayNotifyConfig;
import com.changgou.seckill.service.SeckillOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class WeChatPayNotifyListener {
    @Resource
    private SeckillOrderService orderService;

    /**
     * TODO 消息幂等处理
     *
     * @param message 消息
     */
    @RabbitListener(queues = RabbitMQWeChatPayNotifyConfig.ORDER_PAY_NOTIFY_QUEUE)
    public void receiveMessage(Map<String, String> message) {
        log.info("message: {}", message);
        // 1.接收消息
        // 2.更新对营的订单的状态
        if (message != null) {
            if (message.get("return_code").equalsIgnoreCase("success")) {
                String username = getUsername(message);
                // 格式为yyyyMMddHHmmss
                orderService.update(username, message.get("out_trade_no"), message.get("transaction_id"),
                        getPayTime(message.get("time_end")));
            } else {
                String username = getUsername(message);
                // 删除订单 支付失败
                orderService.delete(username, message.get("out_trade_no"), message.get("transaction_id"),
                        getPayTime(message.get("time_end")));
            }
        }
    }

    private String getUsername(Map<String, String> message) {
        String attach = message.get("attach");
        Map<String, String> map = JSON.parseObject(attach, new TypeReference<Map<String, String>>() {
        });
        return map.get("username");
    }

    private Date getPayTime(String time_end) {
        return Date.from(LocalDateTime.parse(time_end, DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).atZone(ZoneId.systemDefault()).toInstant());
    }

}