package com.changgou.seckill.service.impl;

import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.utils.RandomValueUtil;
import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.bean.SeckillStatus;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.dao.SeckillOrderMapper;
import com.changgou.seckill.service.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * SeckillOrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {
    @Resource
    private SeckillOrderMapper seckillOrderMapper;
    @Resource
    private SeckillGoodsMapper seckillGoodsMapper;
    @Resource
    private SeckillOrderCoreService seckillOrderCoreService;
    @Resource
    private SeckillOrderRedisService seckillOrderRedisService;
    @Resource
    private SeckillOrderQueueService seckillOrderQueueService;
    @Resource
    private SeckillGoodsRedisService seckillGoodsRedisService;
    @Resource
    private SeckillGoodsCountRedisService seckillGoodsCountRedisService;

    @Autowired
    private RedisTemplate<String, Integer> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String getRandomCode(String username) {
        String randomString = RandomValueUtil.getRandomString();
        stringRedisTemplate.boundValueOps("randomCode:" + username).set(randomString, 10, TimeUnit.MINUTES);
        return randomString;
    }

    @Override
    public boolean add(Long id, String time, String username, String random) {
        // 校验密文有效
        String randomCode = stringRedisTemplate.boundValueOps("randomCode:" + username).get();
        if (StringUtils.isEmpty(randomCode) || !random.equals(randomCode)) {
            throw new ChanggouException(ChanggouErrorCode.USELESS_REQUEST);
        }

        String result = preventRepeatCommit(username, id);
        if (result.equalsIgnoreCase("fail")) {
            return false;
        }
        // TODO 高并发，不能查库
        SeckillOrder seckillOrder = seckillOrderMapper.selectByUsername(username, id);
        if (seckillOrder != null) {
            return false;
        }
        return seckillOrderCoreService.create(id, time, username);
    }

    // 防止重复提交
    private String preventRepeatCommit(String username, Long id) {
        String redisKey = "seckill_user_" + username + "_id_" + id;
        Long increment = redisTemplate.opsForValue().increment(redisKey, 1);
        if (increment == null || increment == 1) {
            // 设置有效期五分钟
            redisTemplate.expire(redisKey, 5, TimeUnit.MINUTES);
            return "success";
        }

        return "fail";
    }

    @Override
    public boolean create(Long id, String time, String username) {
        Long count = seckillOrderQueueService.increment(username);
        if (count != null && count > 1) {
            throw new ChanggouException(ChanggouErrorCode.USER_QUEUE_DUPLICATE);
        }

        /*
         * username 抢单的用户是谁
         * status 1 表示抢单的状态 (1.排队中)
         * id       抢的商品的ID
         * time     商品所属时间段
         */
        SeckillStatus seckillStatus = new SeckillStatus(username, 1, id, time, new Date());
        seckillOrderQueueService.leftPush(seckillStatus);
        seckillOrderQueueService.put(username, seckillStatus);

        // 多线程下单 TODO 异常捕获与抛出
        seckillOrderCoreService.createAsync();

        return true;
    }

    @Override
    @Transactional
    public int handleCreateOrder(SeckillOrder order) {
        int result = seckillGoodsMapper.decrease(order.getSeckillId(), 1);
        if (result <= 0) {
            return result;
        }

        result = seckillOrderMapper.insertSelective(order);
        if (result <= 0) {
            return result;
        }

        return 1;
    }

    @Override
    public SeckillStatus status(String username) {
        return seckillOrderQueueService.status(username);
    }

    /**
     * 增加SeckillOrder
     *
     * @param seckillOrder
     */
    @Override
    public void add(SeckillOrder seckillOrder) {
        seckillOrderMapper.insert(seckillOrder);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(Long id) {
        seckillOrderMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void delete(String username, String out_trade_no, String transaction_id, Date time_end) {
        SeckillOrder seckillOrder = seckillOrderRedisService.get(username);
        if (seckillOrder != null) {
            seckillOrder.setPayTime(time_end);
            seckillOrder.setStatus("1");
            seckillOrder.setTransactionId(transaction_id);
            // 订单入库
            seckillOrderMapper.insertSelective(seckillOrder);
            // 删除订单
            seckillOrderRedisService.delete(username);
            // 查询用户排队信息
            SeckillStatus status = seckillOrderQueueService.status(username);
            // 清空排队信息
            seckillOrderQueueService.clearUserQueue(username);
            // 回滚库存
            SeckillGoods seckillGoods = seckillGoodsRedisService.get(status.getTime(), status.getGoodsId());
            if (seckillGoods == null) {
                seckillGoods = seckillGoodsMapper.selectByPrimaryKey(status.getGoodsId());
                seckillGoods.setStockCount(seckillGoods.getStockCount() + 1);
                seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
            } else {
                seckillGoods.setStockCount(seckillGoods.getStockCount() + 1);
                // 注意，这里不同步
                // seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
            }

            // 重新同步到redis
            seckillGoodsRedisService.put(status.getTime(), seckillGoods);
            // 商品计数队列更新
            seckillGoodsCountRedisService.increase(seckillGoods.getId());
            // TODO 关闭微信支付订单
        }
    }

    @Override
    public void close(SeckillStatus status) {
        SeckillStatus seckillStatus = seckillOrderQueueService.status(status.getUsername());
        // 如果没有，什么也不做；如果有，则删除订单
        if (seckillStatus != null) {
            String username = seckillStatus.getUsername();
            // TODO 1.查询支付单，获取交易号和支付时间（为null）
            // 2.删除订单
            SeckillOrder seckillOrder = seckillOrderRedisService.get(username);
            if (seckillOrder != null) {
                seckillOrder.setPayTime(null);
                seckillOrder.setStatus("2");
                seckillOrder.setTransactionId("?");
                // 订单入库
                seckillOrderMapper.insertSelective(seckillOrder);
                // 删除订单
                seckillOrderRedisService.delete(username);
                // 清空排队信息
                seckillOrderQueueService.clearUserQueue(username);
                // 回滚库存
                SeckillGoods seckillGoods = seckillGoodsRedisService.get(status.getTime(), status.getGoodsId());
                if (seckillGoods == null) {
                    seckillGoods = seckillGoodsMapper.selectByPrimaryKey(status.getGoodsId());
                    seckillGoods.setStockCount(seckillGoods.getStockCount() + 1);
                    seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
                } else {
                    seckillGoods.setStockCount(seckillGoods.getStockCount() + 1);
                    // 注意，这里不同步
                    // seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
                }

                // 重新同步到redis
                seckillGoodsRedisService.put(status.getTime(), seckillGoods);
                // 商品计数队列更新
                seckillGoodsCountRedisService.increase(seckillGoods.getId());
                // TODO 3.关闭支付单
            }
        }
    }

    /**
     * 修改SeckillOrder
     *
     * @param seckillOrder
     */
    @Override
    public void update(SeckillOrder seckillOrder) {
        seckillOrderMapper.updateByPrimaryKey(seckillOrder);
    }

    @Override
    public void update(String username, String out_trade_no, String transaction_id, Date time_end) {
        // 修改订单状态
        SeckillOrder seckillOrder = seckillOrderRedisService.get(username);
        if (seckillOrder != null) {
            seckillOrder.setPayTime(time_end);
            seckillOrder.setStatus("1");
            seckillOrder.setTransactionId(transaction_id);
            seckillOrderMapper.insertSelective(seckillOrder);
            // 删除redis中的订单
            seckillOrderRedisService.delete(username);
            // 删除排队信息
            seckillOrderQueueService.clearUserQueue(username);
        }
    }

    /**
     * 根据ID查询SeckillOrder
     *
     * @param id
     * @return
     */
    @Override
    public SeckillOrder findById(Long id) {
        return seckillOrderMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询SeckillOrder全部数据
     *
     * @return
     */
    @Override
    public List<SeckillOrder> findAll() {
        return seckillOrderMapper.selectAll();
    }

    /**
     * SeckillOrder条件查询
     *
     * @param seckillOrder
     * @return
     */
    @Override
    public List<SeckillOrder> findList(SeckillOrder seckillOrder) {
        // 构建查询条件
        Example example = createExample(seckillOrder);
        // 根据构建的条件查询数据
        return seckillOrderMapper.selectByExample(example);
    }

    /**
     * SeckillOrder分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageInfo<SeckillOrder> findPage(int page, int size) {
        // 静态分页
        PageHelper.startPage(page, size);
        // 分页查询
        return new PageInfo<SeckillOrder>(seckillOrderMapper.selectAll());
    }

    /**
     * SeckillOrder条件+分页查询
     *
     * @param seckillOrder 查询条件
     * @param page         页码
     * @param size         页大小
     * @return 分页结果
     */
    @Override
    public PageInfo<SeckillOrder> findPage(SeckillOrder seckillOrder, int page, int size) {
        // 分页
        PageHelper.startPage(page, size);
        // 搜索条件构建
        Example example = createExample(seckillOrder);
        // 执行搜索
        return new PageInfo<SeckillOrder>(seckillOrderMapper.selectByExample(example));
    }

    /**
     * SeckillOrder构建查询对象
     *
     * @param seckillOrder
     * @return
     */
    public Example createExample(SeckillOrder seckillOrder) {
        Example example = new Example(SeckillOrder.class);
        Example.Criteria criteria = example.createCriteria();
        if (seckillOrder != null) {
            // 主键
            if (!StringUtils.isEmpty(seckillOrder.getId())) {
                criteria.andEqualTo("id", seckillOrder.getId());
            }
            // 秒杀商品ID
            if (!StringUtils.isEmpty(seckillOrder.getSeckillId())) {
                criteria.andEqualTo("seckillId", seckillOrder.getSeckillId());
            }
            // 支付金额
            if (!StringUtils.isEmpty(seckillOrder.getMoney())) {
                criteria.andEqualTo("money", seckillOrder.getMoney());
            }
            // 用户
            if (!StringUtils.isEmpty(seckillOrder.getUserId())) {
                criteria.andEqualTo("userId", seckillOrder.getUserId());
            }
            // 创建时间
            if (!StringUtils.isEmpty(seckillOrder.getCreateTime())) {
                criteria.andEqualTo("createTime", seckillOrder.getCreateTime());
            }
            // 支付时间
            if (!StringUtils.isEmpty(seckillOrder.getPayTime())) {
                criteria.andEqualTo("payTime", seckillOrder.getPayTime());
            }
            // 状态，0未支付，1已支付
            if (!StringUtils.isEmpty(seckillOrder.getStatus())) {
                criteria.andEqualTo("status", seckillOrder.getStatus());
            }
            // 收货人地址
            if (!StringUtils.isEmpty(seckillOrder.getReceiverAddress())) {
                criteria.andEqualTo("receiverAddress", seckillOrder.getReceiverAddress());
            }
            // 收货人电话
            if (!StringUtils.isEmpty(seckillOrder.getReceiverMobile())) {
                criteria.andEqualTo("receiverMobile", seckillOrder.getReceiverMobile());
            }
            // 收货人
            if (!StringUtils.isEmpty(seckillOrder.getReceiver())) {
                criteria.andEqualTo("receiver", seckillOrder.getReceiver());
            }
            // 交易流水
            if (!StringUtils.isEmpty(seckillOrder.getTransactionId())) {
                criteria.andEqualTo("transactionId", seckillOrder.getTransactionId());
            }
        }
        return example;
    }

}
