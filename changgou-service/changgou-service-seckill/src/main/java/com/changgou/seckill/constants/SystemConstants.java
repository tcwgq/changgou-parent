package com.changgou.seckill.constants;

/**
 * @version 1.0
 * @since 1.0
 */
public class SystemConstants {
    /**
     * 秒杀商品存储到前缀的KEY
     */
    public static final String SEC_KILL_GOODS_PREFIX = "SeckillGoods:";

    /**
     * 所有的商品计数的大的key(用于存储所有的商品对应的库存数据)
     * bigkey field1(商品ID 1) value(库存数2)
     */
    public static final String SECK_KILL_GOODS_COUNT_KEY = "SeckillGoodsCount:";

    /**
     * 防止超卖的问题的队列的key
     */
    public static final String SECK_KILL_GOODS_LIST_COUNT_KEY = "SeckillGoodsListCount:";

    /**
     * 存储域订单的hash的大key
     */
    public static final String SEC_KILL_ORDER_KEY = "SeckillOrder:map";

    /**
     * 用户排队的队列的KEY
     */
    public static final String SEC_KILL_USER_QUEUE_KEY = "SeckillOrderQueue:";

    /**
     * 用户排队标识的key (用于存储谁买了什么商品以及抢单的状态)
     */
    public static final String SEC_KILL_USER_STATUS_KEY = "UserQueueStatus:";

    /**
     * 用于防止重复排队的hash的key的值
     */
    public static final String SEC_KILL_QUEUE_REPEAT_KEY = "UserQueueCount:";

}
