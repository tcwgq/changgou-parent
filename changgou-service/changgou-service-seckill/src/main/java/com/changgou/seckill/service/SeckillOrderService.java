package com.changgou.seckill.service;

import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.bean.SeckillStatus;
import com.github.pagehelper.PageInfo;

import java.util.Date;
import java.util.List;

/**
 * SeckillOrderService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillOrderService {
    String getRandomCode(String username);

    boolean add(Long id, String time, String username, String random);

    boolean create(Long id, String time, String username);

    int handleCreateOrder(SeckillOrder order);

    SeckillStatus status(String username);

    /**
     * 新增SeckillOrder
     *
     * @param seckillOrder
     */
    void add(SeckillOrder seckillOrder);

    /**
     * 删除SeckillOrder
     *
     * @param id
     */
    void delete(Long id);

    void delete(String username, String out_trade_no, String transaction_id, Date time_end);

    void close(SeckillStatus status);

    /**
     * 修改SeckillOrder数据
     *
     * @param seckillOrder
     */
    void update(SeckillOrder seckillOrder);

    void update(String username, String out_trade_no, String transaction_id, Date time_end);

    /**
     * 根据ID查询SeckillOrder
     *
     * @param id
     * @return
     */
    SeckillOrder findById(Long id);

    /**
     * 查询所有SeckillOrder
     *
     * @return
     */
    List<SeckillOrder> findAll();

    /**
     * SeckillOrder多条件搜索方法
     *
     * @param seckillOrder
     * @return
     */
    List<SeckillOrder> findList(SeckillOrder seckillOrder);

    /**
     * SeckillOrder分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<SeckillOrder> findPage(int page, int size);

    /**
     * SeckillOrder多条件分页查询
     *
     * @param seckillOrder
     * @param page
     * @param size
     * @return
     */
    PageInfo<SeckillOrder> findPage(SeckillOrder seckillOrder, int page, int size);

}
