package com.changgou.oauth.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.oauth.bean.AuthToken;
import com.changgou.oauth.service.AuthService;
import com.changgou.oauth.util.CookieUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @since 2019/7/7 16:42
 */
@Controller
@RequestMapping(value = "/oauth")
public class AuthController {
    public static final String Authorization = "Authorization";

    @Resource
    AuthService authService;
    // 客户端ID
    @Value("${auth.clientId}")
    private String clientId;
    // 秘钥
    @Value("${auth.clientSecret}")
    private String clientSecret;
    // Cookie存储的域名
    @Value("${auth.cookieDomain}")
    private String cookieDomain;
    // Cookie生命周期
    @Value("${auth.cookieMaxAge}")
    private int cookieMaxAge;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @ResponseBody
    @PostMapping("/login")
    public Result<AuthToken> login(@RequestParam String username, @RequestParam String password) {
        // 申请令牌
        AuthToken authToken = authService.login(username, password, clientId, clientSecret);

        // 用户身份令牌
        String access_token = authToken.getAccessToken();
        // 将令牌存储到cookie
        saveCookie(access_token);
        // 将令牌存到redis
        saveRedis(access_token);

        return new Result<>(true, StatusCode.OK, "登录成功！", authToken);
    }

    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        String token = getToken(request);
        if (StringUtils.isNotEmpty(token)) {
            if (token.startsWith("bearer ") || token.startsWith("Bearer ")) {
                token = token.substring(7);
            }
            // TODO 如何从TokenDecoder中获取token
            // 报错org.springframework.security.web.authentication.WebAuthenticationDetails cannot be cast to
            // org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
            // String token = TokenDecoder.getToken();
            // 删除redis中的token
            redisTemplate.delete(token);
            // 删除cookie中的token
            deleteCookie(response, token);
        }

        // 跳转到登陆也
        return "redirect:/toLogin";// 重定向发送manage.html请求
    }

    /**
     * 将令牌存储到cookie
     */
    private void saveCookie(String token) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
            if (response != null) {
                CookieUtil.addCookie(response, cookieDomain, "/", "Authorization", token, cookieMaxAge, false);
            }
        }
    }

    private void saveRedis(String token) {
        redisTemplate.boundValueOps(token).set(token);
    }

    private String getToken(HttpServletRequest request) {
        String first = request.getHeader(Authorization);
        if (StringUtils.isNotEmpty(first)) {
            return first;
        }

        String second = request.getParameter(Authorization);
        if (StringUtils.isNotEmpty(second)) {
            return second;
        }

        String third = "";
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equalsIgnoreCase("authorization")) {
                third = cookie.getValue();
            }
        }

        return third;
    }

    private void deleteCookie(HttpServletResponse response, String token) {
        Cookie cookie = new Cookie("Authorization", token); // 假如要删除名称为username的Cookie
        cookie.setMaxAge(0); // 立即删除型
        cookie.setDomain("localhost");// 保存cookie的IP地址,则是删除这个IP的cookie
        cookie.setPath("/"); // 项目所有目录均有效，这句很关键，否则不敢保证删除
        cookie.setHttpOnly(true);
        response.addCookie(cookie); // 重新写入，将覆盖之前的
    }

}
