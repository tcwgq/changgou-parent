package com.changgou.oauth.service.impl;

import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.oauth.bean.AuthToken;
import com.changgou.oauth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;

/**
 * @since 2019/7/7 16:23
 */
@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 授权认证方法
     *
     * @param username
     * @param password
     * @param clientId
     * @param clientSecret
     * @return
     */
    @Override
    public AuthToken login(String username, String password, String clientId, String clientSecret) {
        // 申请令牌
        return applyToken(username, password, clientId, clientSecret, "password");
    }

    /**
     * 认证方法
     *
     * @param username:用户登录名字
     * @param password：用户密码
     * @param clientId：配置文件中的客户端ID
     * @param clientSecret：配置文件中的秘钥
     * @return
     */
    private AuthToken applyToken(String username, String password, String clientId, String clientSecret, String grantType) {
        // 参数:微服务的名称spring.application指定的名称
        ServiceInstance serviceInstance = loadBalancerClient.choose("user-auth-service");
        if (serviceInstance == null) {
            throw new ChanggouException(ChanggouErrorCode.CAN_FIND_SERVICE);
        }
        // 获取令牌的url
        String path = serviceInstance.getUri().toString() + "/oauth/token";
        // 定义头
        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
        header.add("Authorization", httpBasic(clientId, clientSecret));
        // 定义body
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        // 授权方式
        formData.add("grant_type", grantType);
        // 账号
        formData.add("username", username);
        // 密码
        formData.add("password", password);
        // 指定 restTemplate当遇到400或401响应时候也不要抛出异常，也要正常返回值
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public void handleError(@NonNull ClientHttpResponse response) throws IOException {
                // 当响应的值为400或401时候也要正常响应，不要抛出异常
                if (response.getRawStatusCode() != 400 && response.getRawStatusCode() != 401) {
                    super.handleError(response);
                }
            }
        });
        Map<String, String> map;
        try {
            // http请求spring security的申请令牌接口
            ResponseEntity<Map<String, String>> mapResponseEntity = restTemplate.exchange(path, HttpMethod.POST, new HttpEntity<>(formData, header), new ParameterizedTypeReference<Map<String, String>>() {

            });
            // 获取响应数据
            map = mapResponseEntity.getBody();
        } catch (RestClientException e) {
            throw new ChanggouException(ChanggouErrorCode.CREATE_TOKEN_RESPONSE_FAIL);
        }
        if (map == null || map.get("access_token") == null || map.get("refresh_token") == null || map.get("jti") == null) {
            // jti是jwt令牌的唯一标识作为用户身份令牌
            throw new ChanggouException(ChanggouErrorCode.CREATE_TOKEN_FAIL);
        }

        // 将响应数据封装成AuthToken对象
        AuthToken authToken = new AuthToken();
        // 访问令牌(jwt)
        String accessToken = map.get("access_token");
        // 刷新令牌(jwt)
        String refreshToken = map.get("refresh_token");
        // jti，作为用户的身份标识
        String jwtToken = map.get("jti");
        authToken.setJti(jwtToken);
        authToken.setAccessToken(accessToken);
        authToken.setRefreshToken(refreshToken);
        return authToken;
    }

    /**
     * base64编码
     *
     * @param clientId
     * @param clientSecret
     * @return Base64编码
     */
    private String httpBasic(String clientId, String clientSecret) {
        // 将客户端id和客户端密码拼接，按“客户端id:客户端密码”
        String string = clientId + ":" + clientSecret;
        // 进行base64编码
        byte[] encode = Base64Utils.encode(string.getBytes());
        return "Basic " + new String(encode);
    }

}
