package com.changgou.file.controller;

import com.changgou.common.response.Result;
import com.changgou.file.bean.FastDFSFile;
import com.changgou.file.service.UploadService;
import com.changgou.file.utils.FastDFSClient;
import org.csource.fastdfs.FileInfo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author tcwgq
 * @since 2022/5/27 14:45
 */
@RestController
@RequestMapping("/upload")
public class UploadController {
    @Resource
    private UploadService uploadService;

    @PostMapping
    public Result<String> upload(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        String extName = StringUtils.getFilenameExtension(originalFilename);
        FastDFSFile fastDFSFile = new FastDFSFile(originalFilename, bytes, extName);
        return Result.success(uploadService.upload(fastDFSFile));
    }

    @GetMapping("/info")
    public Result<FileInfo> upload(@RequestParam("group") String group, @RequestParam("filename") String filename) {
        return Result.success(FastDFSClient.getFile(group, filename));
    }

}
