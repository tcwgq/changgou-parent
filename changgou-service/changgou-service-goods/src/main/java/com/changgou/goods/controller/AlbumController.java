package com.changgou.goods.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Album;
import com.changgou.goods.service.AlbumService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/27 17:58
 */
@RestController
@RequestMapping("/album")
@CrossOrigin
public class AlbumController {
    @Resource
    private AlbumService albumService;

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Album album) {
        return Result.success(albumService.add(album));
    }

    @DeleteMapping("/{id}")
    public Result<Integer> delete(@PathVariable("id") Long id) {
        return Result.success(albumService.delete(id));
    }

    @PutMapping("/{id}")
    public Result<Integer> updateById(@PathVariable("id") Long id, @RequestBody Album album) {
        return Result.success(albumService.updateById(id, album));
    }

    @PostMapping("/update")
    public Result<Integer> update(@RequestBody Album album) {
        return Result.success(albumService.update(album));
    }

    @GetMapping("/{id}")
    public Result<Album> findById(@PathVariable("id") Long id) {
        return Result.success(albumService.findById(id));
    }

    @PostMapping("/search")
    public Result<List<Album>> search(@RequestBody Album album) {
        return Result.success(albumService.findList(album));
    }

    @GetMapping("/findAll")
    public Result<List<Album>> findAll() {
        return Result.success(albumService.findAll());
    }

    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Album>> page(@PathVariable("page") Integer page,
                                        @PathVariable("size") Integer size) {
        return Result.success(albumService.page(page, size));
    }

    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Album>> page(@PathVariable("page") Integer page,
                                        @PathVariable("size") Integer size,
                                        @RequestBody Album album) {
        return Result.success(albumService.page(page, size, album));
    }

}
