package com.changgou.goods.service.impl;

import com.changgou.goods.dao.AlbumMapper;
import com.changgou.goods.pojo.Album;
import com.changgou.goods.service.AlbumService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
@Service
public class AlbumServiceImpl implements AlbumService {
    @Resource
    private AlbumMapper albumMapper;

    @Override
    public int add(Album album) {
        return albumMapper.insertSelective(album);
    }

    @Override
    public int delete(Long id) {
        return albumMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Long id, Album album) {
        album.setId(id);
        return albumMapper.updateByPrimaryKeySelective(album);
    }

    @Override
    public int update(Album album) {
        return albumMapper.updateByPrimaryKeySelective(album);
    }

    @Override
    public Album findById(Long id) {
        return albumMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Album> findList(Album album) {
        Example example = buildExample(album);
        return albumMapper.selectByExample(example);
    }

    @Override
    public List<Album> findAll() {
        return albumMapper.selectAll();
    }

    @Override
    public PageInfo<Album> page(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<Album> albums = albumMapper.selectAll();
        return new PageInfo<>(albums);
    }

    @Override
    public PageInfo<Album> page(Integer page, Integer size, Album album) {
        PageHelper.startPage(page, size);
        Example example = buildExample(album);
        List<Album> albums = albumMapper.selectByExample(example);
        return new PageInfo<>(albums);
    }

    private Example buildExample(Album album) {
        Example example = new Example(Album.class);
        Example.Criteria criteria = example.createCriteria();
        if (album != null) {
            if (album.getId() != null) {
                criteria.andEqualTo("id", album.getId());
            }
            if (StringUtils.isNotBlank(album.getTitle())) {
                criteria.andLike("title", "%" + album.getTitle() + "%");
            }
        }
        return example;
    }

}
