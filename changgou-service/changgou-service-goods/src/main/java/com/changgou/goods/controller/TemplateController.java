package com.changgou.goods.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Template;
import com.changgou.goods.service.TemplateService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@RestController
@RequestMapping("/template")
@CrossOrigin
public class TemplateController {
    @Resource
    private TemplateService templateService;

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Template template) {
        return Result.success(templateService.add(template));
    }

    @DeleteMapping("/{id}")
    public Result<Integer> delete(@PathVariable("id") Long id) {
        return Result.success(templateService.delete(id));
    }

    @PutMapping("/{id}")
    public Result<Integer> updateById(@PathVariable("id") Long id, @RequestBody Template template) {
        return Result.success(templateService.updateById(id, template));
    }

    @PostMapping("/update")
    public Result<Integer> update(@RequestBody Template template) {
        return Result.success(templateService.update(template));
    }

    @GetMapping("/{id}")
    public Result<Template> findById(@PathVariable("id") Long id) {
        return Result.success(templateService.findById(id));
    }

    @GetMapping("/findAll")
    public Result<List<Template>> findAll() {
        return Result.success(templateService.findAll());
    }

    @PostMapping("/search")
    public Result<List<Template>> search(@RequestBody Template template) {
        return Result.success(templateService.findList(template));
    }

    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Template>> page(@PathVariable("page") Integer page,
                                           @PathVariable("size") Integer size) {
        return Result.success(templateService.page(page, size));
    }

    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Template>> page(@PathVariable("page") Integer page,
                                           @PathVariable("size") Integer size,
                                           @RequestBody Template template) {
        return Result.success(templateService.page(page, size, template));
    }

}
