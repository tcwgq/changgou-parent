package com.changgou.goods.service;

import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface BrandService {
    int add(Brand brand);

    int delete(Long id);

    int updateById(Long id, Brand brand);

    int update(Brand brand);

    Brand findById(Long id);

    List<Brand> findByCategoryId(Long categoryId);

    List<Brand> findList(Brand brand);

    List<Brand> findAll();

    PageInfo<Brand> page(Integer page, Integer size);

    PageInfo<Brand> page(Integer page, Integer size, Brand brand);
}
