package com.changgou.goods.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.goods.pojo.CategoryBrand;
import com.changgou.goods.service.CategoryBrandService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Api(value = "CategoryBrandController")
@RestController
@RequestMapping("/categoryBrand")
@CrossOrigin
public class CategoryBrandController {
    @Autowired
    private CategoryBrandService categoryBrandService;

    @PostMapping
    public Result<String> add(@RequestBody CategoryBrand categoryBrand) {
        // 调用CategoryBrandService实现添加CategoryBrand
        categoryBrandService.add(categoryBrand);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用CategoryBrandService实现根据主键删除
        categoryBrandService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody CategoryBrand categoryBrand, @PathVariable Long id) {
        // 设置主键值
        categoryBrand.setCategoryId(id);
        // 调用CategoryBrandService实现修改CategoryBrand
        categoryBrandService.update(categoryBrand);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    @GetMapping("/{id}")
    public Result<CategoryBrand> findById(@PathVariable Long id) {
        // 调用CategoryBrandService实现根据主键查询CategoryBrand
        CategoryBrand categoryBrand = categoryBrandService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", categoryBrand);
    }

    @GetMapping
    public Result<List<CategoryBrand>> findAll() {
        // 调用CategoryBrandService实现查询所有CategoryBrand
        List<CategoryBrand> list = categoryBrandService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @PostMapping(value = "/search")
    public Result<List<CategoryBrand>> findList(@RequestBody(required = false) CategoryBrand categoryBrand) {
        // 调用CategoryBrandService实现条件查询CategoryBrand
        List<CategoryBrand> list = categoryBrandService.findList(categoryBrand);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<CategoryBrand>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用CategoryBrandService实现分页查询CategoryBrand
        PageInfo<CategoryBrand> pageInfo = categoryBrandService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<CategoryBrand>> findPage(@RequestBody(required = false) CategoryBrand categoryBrand, @PathVariable int page, @PathVariable int size) {
        // 调用CategoryBrandService实现分页条件查询CategoryBrand
        PageInfo<CategoryBrand> pageInfo = categoryBrandService.findPage(categoryBrand, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
