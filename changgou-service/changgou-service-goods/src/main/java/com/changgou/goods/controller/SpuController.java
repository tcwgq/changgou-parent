package com.changgou.goods.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import com.changgou.goods.service.SpuService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Api(value = "SpuController")
@RestController
@RequestMapping("/spu")
@CrossOrigin
public class SpuController {
    @Resource
    private SpuService spuService;

    @PostMapping
    public Result<String> add(@RequestBody Spu spu) {
        // 调用SpuService实现添加Spu
        spuService.add(spu);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * Goods(SPU+SKU)增加方法详情
     */
    @PostMapping("/save")
    public Result<String> save(@RequestBody Goods goods) {
        // 调用SpuService实现添加goods
        spuService.save(goods);
        return new Result<>(true, StatusCode.OK, "保存商品成功");
    }

    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用SpuService实现根据主键删除
        spuService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody Spu spu, @PathVariable Long id) {
        // 设置主键值
        spu.setId(id);
        // 调用SpuService实现修改Spu
        spuService.update(spu);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 审核商品 自动上架
     */
    @PutMapping("/audit/{id}")
    public Result<String> audit(@PathVariable(name = "id") Long id) {
        spuService.audit(id);
        return new Result<>(true, StatusCode.OK, "审核通过");
    }

    @PutMapping("/pull/{id}")
    public Result<String> pull(@PathVariable(name = "id") Long id) {
        spuService.pull(id);
        return new Result<>(true, StatusCode.OK, "下架成功");
    }

    @PutMapping("/put/{id}")
    public Result<String> put(@PathVariable(name = "id") Long id) {
        spuService.put(id);
        return new Result<>(true, StatusCode.OK, "上架成功");
    }

    @PutMapping("/put/many")
    public Result<String> putMany(@RequestBody Long[] ids) {
        spuService.putMany(ids);
        return new Result<>(true, StatusCode.OK, "批量上架成功");
    }

    @DeleteMapping("/logic/delete/{id}")
    public Result<String> logicDeleteSpu(@PathVariable(name = "id") Long id) {
        spuService.logicDeleteSpu(id);
        return new Result<>(true, StatusCode.OK, "逻辑删除成功");
    }

    @PutMapping("/restore/{id}")
    public Result<String> restore(@PathVariable(name = "id") Long id) {
        spuService.restoreSpu(id);
        return new Result<>(true, StatusCode.OK, "还原成功");
    }

    @GetMapping("/{id}")
    public Result<Spu> findById(@PathVariable Long id) {
        // 调用SpuService实现根据主键查询Spu
        Spu spu = spuService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", spu);
    }

    @GetMapping("/goods/{id}")
    public Result<Goods> findGoodsById(@PathVariable Long id) {
        // 调用SpuService实现根据主键查询Spu
        Goods goods = spuService.findGoodsById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", goods);
    }

    @GetMapping
    public Result<List<Spu>> findAll() {
        // 调用SpuService实现查询所有Spu
        List<Spu> list = spuService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @PostMapping(value = "/search")
    public Result<List<Spu>> findList(@RequestBody(required = false) Spu spu) {
        // 调用SpuService实现条件查询Spu
        List<Spu> list = spuService.findList(spu);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Spu>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用SpuService实现分页查询Spu
        PageInfo<Spu> pageInfo = spuService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Spu>> findPage(@RequestBody(required = false) Spu spu, @PathVariable int page, @PathVariable int size) {
        // 调用SpuService实现分页条件查询Spu
        PageInfo<Spu> pageInfo = spuService.findPage(spu, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
