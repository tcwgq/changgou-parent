package com.changgou.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.utils.IdWorker;
import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.dao.CategoryMapper;
import com.changgou.goods.dao.SkuMapper;
import com.changgou.goods.dao.SpuMapper;
import com.changgou.goods.pojo.*;
import com.changgou.goods.service.SpuService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Service
public class SpuServiceImpl implements SpuService {
    @Resource
    private SpuMapper spuMapper;

    @Resource
    private SkuMapper skuMapper;

    @Resource
    private BrandMapper brandMapper;

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private IdWorker idWorker;

    /**
     * 增加Spu
     *
     * @param spu
     */
    @Override
    public void add(Spu spu) {
        spuMapper.insert(spu);
    }

    @Override
    public void save(Goods goods) {
        Spu spu = goods.getSpu();
        if (spu.getId() == null) {
            spu.setId(idWorker.nextId());
            spu.setIsDelete("0");
            spu.setIsMarketable("0");
            spu.setStatus("0");
            spuMapper.insertSelective(spu);
        } else {
            spuMapper.updateByPrimaryKeySelective(spu);
            Sku sku = new Sku();
            sku.setSpuId(spu.getId());
            skuMapper.delete(sku);
        }

        List<Sku> skuList = goods.getSkuList();
        Category category = categoryMapper.selectByPrimaryKey(spu.getCategory3Id());
        Brand brand = brandMapper.selectByPrimaryKey(spu.getBrandId());
        Date date = new Date();
        for (Sku sku : skuList) {
            sku.setId(idWorker.nextId());
            if (StringUtils.isEmpty(sku.getSpec())) {
                sku.setSpec("{}");
            }
            Map<String, String> map = JSON.parseObject(sku.getSpec(), new TypeReference<Map<String, String>>() {
            });
            String suffix = String.join(" ", map.values());
            sku.setName(spu.getName() + " " + suffix);
            sku.setCreateTime(date);
            sku.setUpdateTime(date);
            sku.setSpuId(spu.getId());
            sku.setCategoryId(sku.getCategoryId());
            sku.setCategoryName(category.getName());
            sku.setBrandName(brand.getName());
            sku.setSpec(sku.getSpec());
            sku.setStatus("1");
            skuMapper.insertSelective(sku);
        }
    }

    @Override
    public void delete(Long id) {
        spuMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(Spu spu) {
        spuMapper.updateByPrimaryKey(spu);
    }

    @Override
    public void audit(Long id) {
        // update tb_spu set status=1, is_marketable = 1 where is_delete = 0 and id = ?
        // 先判断是否已经被删除
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null || spu.getIsDelete().equals("1")) {// 已经被删除了或者商品不存在
            throw new ChanggouException(ChanggouErrorCode.GOOD_NOT_EXIST_OR_DELETE);
        }
        // 审核商品
        spu.setStatus("1");// 已经审核
        spu.setIsMarketable("1");// 自动上架
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void pull(Long id) {
        // update tb_spu set is_marketable=0 where is_delete=0 and id = ? and is_marketable=1 and status=1
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null || spu.getIsDelete().equals("1")) {// 已经被删除了或者商品不存在
            throw new ChanggouException(ChanggouErrorCode.GOOD_NOT_EXIST_OR_DELETE);
        }

        if (!spu.getStatus().equals("1") || !spu.getIsMarketable().equals("1")) {
            throw new ChanggouException(ChanggouErrorCode.GOOD_MUST_ON);
        }

        spu.setIsMarketable("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void put(Long id) {
        // update tb_spu set is_marketable=1 where is_delete=0 and id = ? and is_marketable=0 and status=1
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null || spu.getIsDelete().equals("1")) {// 已经被删除了或者商品不存在
            throw new ChanggouException(ChanggouErrorCode.GOOD_NOT_EXIST_OR_DELETE);
        }

        if (!spu.getStatus().equals("1") || !spu.getIsMarketable().equals("0")) {
            throw new ChanggouException(ChanggouErrorCode.GOOD_MUST_ON1);
        }

        spu.setIsMarketable("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void putMany(Long[] ids) {
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", Arrays.asList(ids));
        criteria.andEqualTo("isDelete", "0");
        criteria.andEqualTo("status", "1");
        criteria.andEqualTo("isMarketable", "0");
        Spu spu = new Spu();
        spu.setIsMarketable("1");
        spuMapper.updateByExampleSelective(spu, example);
    }

    @Override
    public void logicDeleteSpu(Long id) {
        // update set is_delete=1 where id =? and is_delete=0
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null) {
            throw new ChanggouException(ChanggouErrorCode.GOOD_NOT_EXIST);
        }

        if (spu.getIsMarketable().equals("1")) {
            throw new ChanggouException(ChanggouErrorCode.GOOD_ON_SAIL);
        }
        spu.setIsDelete("1");
        spu.setStatus("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void restoreSpu(Long id) {
        // update set is_delete=0 where id =? and is_delete=1
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null) {
            throw new ChanggouException(ChanggouErrorCode.GOOD_NOT_EXIST);
        }
        Spu data = new Spu();
        data.setIsDelete("0");// 恢复
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", id);// where id =1
        criteria.andEqualTo("isDelete", "1");
        spuMapper.updateByExampleSelective(data, example);
        // spuMapper.updateByPrimaryKeySelective(spu);// 根据主键来进行更新  update set name = 'zhangSan' where id = ?
    }

    @Override
    public Spu findById(Long id) {
        return spuMapper.selectByPrimaryKey(id);
    }

    @Override
    public Goods findGoodsById(Long spuId) {
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        Sku sku = new Sku();
        sku.setSpuId(spuId);
        List<Sku> skus = skuMapper.select(sku);
        Goods goods = new Goods();
        goods.setSpu(spu);
        goods.setSkuList(skus);
        return goods;
    }

    @Override
    public List<Spu> findAll() {
        return spuMapper.selectAll();
    }

    @Override
    public List<Spu> findList(Spu spu) {
        // 构建查询条件
        Example example = createExample(spu);
        // 根据构建的条件查询数据
        return spuMapper.selectByExample(example);
    }

    @Override
    public PageInfo<Spu> findPage(int page, int size) {
        // 静态分页
        PageHelper.startPage(page, size);
        // 分页查询
        return new PageInfo<>(spuMapper.selectAll());
    }

    @Override
    public PageInfo<Spu> findPage(Spu spu, int page, int size) {
        // 分页
        PageHelper.startPage(page, size);
        // 搜索条件构建
        Example example = createExample(spu);
        // 执行搜索
        return new PageInfo<>(spuMapper.selectByExample(example));
    }

    public Example createExample(Spu spu) {
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        if (spu != null) {
            // 主键
            if (!StringUtils.isEmpty(spu.getId())) {
                criteria.andEqualTo("id", spu.getId());
            }
            // 货号
            if (!StringUtils.isEmpty(spu.getSn())) {
                criteria.andEqualTo("sn", spu.getSn());
            }
            // SPU名
            if (!StringUtils.isEmpty(spu.getName())) {
                criteria.andLike("name", "%" + spu.getName() + "%");
            }
            // 副标题
            if (!StringUtils.isEmpty(spu.getCaption())) {
                criteria.andEqualTo("caption", spu.getCaption());
            }
            // 品牌ID
            if (!StringUtils.isEmpty(spu.getBrandId())) {
                criteria.andEqualTo("brandId", spu.getBrandId());
            }
            // 一级分类
            if (!StringUtils.isEmpty(spu.getCategory1Id())) {
                criteria.andEqualTo("category1Id", spu.getCategory1Id());
            }
            // 二级分类
            if (!StringUtils.isEmpty(spu.getCategory2Id())) {
                criteria.andEqualTo("category2Id", spu.getCategory2Id());
            }
            // 三级分类
            if (!StringUtils.isEmpty(spu.getCategory3Id())) {
                criteria.andEqualTo("category3Id", spu.getCategory3Id());
            }
            // 模板ID
            if (!StringUtils.isEmpty(spu.getTemplateId())) {
                criteria.andEqualTo("templateId", spu.getTemplateId());
            }
            // 运费模板id
            if (!StringUtils.isEmpty(spu.getFreightId())) {
                criteria.andEqualTo("freightId", spu.getFreightId());
            }
            // 图片
            if (!StringUtils.isEmpty(spu.getImage())) {
                criteria.andEqualTo("image", spu.getImage());
            }
            // 图片列表
            if (!StringUtils.isEmpty(spu.getImages())) {
                criteria.andEqualTo("images", spu.getImages());
            }
            // 售后服务
            if (!StringUtils.isEmpty(spu.getSaleService())) {
                criteria.andEqualTo("saleService", spu.getSaleService());
            }
            // 介绍
            if (!StringUtils.isEmpty(spu.getIntroduction())) {
                criteria.andEqualTo("introduction", spu.getIntroduction());
            }
            // 规格列表
            if (!StringUtils.isEmpty(spu.getSpecItems())) {
                criteria.andEqualTo("specItems", spu.getSpecItems());
            }
            // 参数列表
            if (!StringUtils.isEmpty(spu.getParaItems())) {
                criteria.andEqualTo("paraItems", spu.getParaItems());
            }
            // 销量
            if (!StringUtils.isEmpty(spu.getSaleNum())) {
                criteria.andEqualTo("saleNum", spu.getSaleNum());
            }
            // 评论数
            if (!StringUtils.isEmpty(spu.getCommentNum())) {
                criteria.andEqualTo("commentNum", spu.getCommentNum());
            }
            // 是否上架,0已下架，1已上架
            if (!StringUtils.isEmpty(spu.getIsMarketable())) {
                criteria.andEqualTo("isMarketable", spu.getIsMarketable());
            }
            // 是否启用规格
            if (!StringUtils.isEmpty(spu.getIsEnableSpec())) {
                criteria.andEqualTo("isEnableSpec", spu.getIsEnableSpec());
            }
            // 是否删除,0:未删除，1：已删除
            if (!StringUtils.isEmpty(spu.getIsDelete())) {
                criteria.andEqualTo("isDelete", spu.getIsDelete());
            }
            // 审核状态，0：未审核，1：已审核，2：审核不通过
            if (!StringUtils.isEmpty(spu.getStatus())) {
                criteria.andEqualTo("status", spu.getStatus());
            }
        }
        return example;
    }

}
