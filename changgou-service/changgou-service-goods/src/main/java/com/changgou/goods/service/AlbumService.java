package com.changgou.goods.service;

import com.changgou.goods.pojo.Album;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface AlbumService {
    int add(Album album);

    int delete(Long id);

    int updateById(Long id, Album album);

    int update(Album album);

    Album findById(Long id);

    List<Album> findList(Album album);

    List<Album> findAll();

    PageInfo<Album> page(Integer page, Integer size);

    PageInfo<Album> page(Integer page, Integer size, Album album);
}
