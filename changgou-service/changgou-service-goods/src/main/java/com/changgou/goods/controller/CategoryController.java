package com.changgou.goods.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Category;
import com.changgou.goods.service.CategoryService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Api(value = "CategoryController")
@RestController
@RequestMapping("/category")
@CrossOrigin
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public Result<String> add(@RequestBody Category category) {
        // 调用CategoryService实现添加Category
        categoryService.add(category);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用CategoryService实现根据主键删除
        categoryService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody Category category, @PathVariable Long id) {
        // 设置主键值
        category.setId(id);
        // 调用CategoryService实现修改Category
        categoryService.update(category);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    @GetMapping("/{id}")
    public Result<Category> findById(@PathVariable Long id) {
        // 调用CategoryService实现根据主键查询Category
        Category category = categoryService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", category);
    }

    @GetMapping("/list/{pid}")
    public Result<List<Category>> findByPid(@PathVariable Long pid) {
        List<Category> categories = categoryService.findByParentId(pid);
        return new Result<>(true, StatusCode.OK, "查询成功", categories);
    }

    @GetMapping
    public Result<List<Category>> findAll() {
        // 调用CategoryService实现查询所有Category
        List<Category> list = categoryService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @PostMapping(value = "/search")
    public Result<List<Category>> findList(@RequestBody(required = false) Category category) {
        // 调用CategoryService实现条件查询Category
        List<Category> list = categoryService.findList(category);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Category>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用CategoryService实现分页查询Category
        PageInfo<Category> pageInfo = categoryService.page(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Category>> findPage(@RequestBody(required = false) Category category, @PathVariable int page, @PathVariable int size) {
        // 调用CategoryService实现分页条件查询Category
        PageInfo<Category> pageInfo = categoryService.page(page, size, category);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
