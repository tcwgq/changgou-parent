package com.changgou.goods.service.impl;

import com.changgou.goods.dao.ParaMapper;
import com.changgou.goods.pojo.Para;
import com.changgou.goods.service.ParaService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
@Service
public class ParaServiceImpl implements ParaService {
    @Resource
    private ParaMapper paraMapper;

    @Override
    public int add(Para para) {
        return paraMapper.insertSelective(para);
    }

    @Override
    public int delete(Long id) {
        return paraMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Long id, Para para) {
        para.setId(id);
        return paraMapper.updateByPrimaryKeySelective(para);
    }

    @Override
    public int update(Para para) {
        return paraMapper.updateByPrimaryKeySelective(para);
    }

    @Override
    public Para findById(Long id) {
        return paraMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Para> findByCategoryId(Long categoryId) {
        return paraMapper.findByCategoryId(categoryId);
    }

    @Override
    public List<Para> findList(Para para) {
        Example example = buildExample(para);
        return paraMapper.selectByExample(example);
    }

    @Override
    public List<Para> findAll() {
        return paraMapper.selectAll();
    }

    @Override
    public PageInfo<Para> page(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<Para> paras = paraMapper.selectAll();
        return new PageInfo<>(paras);
    }

    @Override
    public PageInfo<Para> page(Integer page, Integer size, Para para) {
        PageHelper.startPage(page, size);
        Example example = buildExample(para);
        List<Para> paras = paraMapper.selectByExample(example);
        return new PageInfo<>(paras);
    }

    private Example buildExample(Para para) {
        Example example = new Example(Para.class);
        Example.Criteria criteria = example.createCriteria();
        if (para != null) {
            if (para.getId() != null) {
                criteria.andEqualTo("id", para.getId());
            }
            if (StringUtils.isNotBlank(para.getName())) {
                criteria.andLike("name", "%" + para.getName() + "%");
            }
        }
        return example;
    }

}
