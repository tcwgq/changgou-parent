package com.changgou.goods.dao;

import com.changgou.goods.pojo.Spec;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 通用mapper实现
 *
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface SpecMapper extends Mapper<Spec> {
    @Select("select ts.* from tb_category tc, tb_spec ts where tc.template_id = ts.template_id and tc.id = #{categoryId};")
    List<Spec> findByCategoryId(@Param("categoryId") Long categoryId);

}
