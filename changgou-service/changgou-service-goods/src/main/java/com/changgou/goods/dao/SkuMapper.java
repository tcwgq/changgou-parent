package com.changgou.goods.dao;

import com.changgou.goods.pojo.Sku;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface SkuMapper extends Mapper<Sku> {
    @Update(value = "update tb_sku set num = num - #{num}, sale_num = sale_num + #{num} where id = #{skuId} and num >= #{num}")
    int decrease(@Param("skuId") Long skuId, @Param("num") Integer num);

    @Update(value = "update tb_sku set num = num + #{num}, sale_num = sale_num - #{num} where id = #{skuId} and sale_num >= #{num}")
    int increase(@Param("skuId") Long skuId, @Param("num") Integer num);

}
