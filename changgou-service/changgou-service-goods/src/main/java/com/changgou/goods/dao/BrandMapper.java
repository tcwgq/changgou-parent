package com.changgou.goods.dao;

import com.changgou.goods.pojo.Brand;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 通用mapper实现
 *
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface BrandMapper extends Mapper<Brand> {
    @Select("select tb.* from tb_brand tb, tb_category_brand tcb where tb.id = tcb.brand_id and tcb.category_id = #{categoryId}")
    List<Brand> findByCategoryId(@Param("categoryId") Long categoryId);

}
