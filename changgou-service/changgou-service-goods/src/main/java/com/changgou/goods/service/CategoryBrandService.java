package com.changgou.goods.service;

import com.changgou.goods.pojo.CategoryBrand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface CategoryBrandService {
    void add(CategoryBrand categoryBrand);

    void delete(Long id);

    void update(CategoryBrand categoryBrand);

    CategoryBrand findById(Long id);

    List<CategoryBrand> findAll();

    List<CategoryBrand> findList(CategoryBrand categoryBrand);

    PageInfo<CategoryBrand> findPage(int page, int size);

    PageInfo<CategoryBrand> findPage(CategoryBrand categoryBrand, int page, int size);

}
