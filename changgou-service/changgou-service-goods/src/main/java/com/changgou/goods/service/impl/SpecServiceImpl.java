package com.changgou.goods.service.impl;

import com.changgou.goods.dao.SpecMapper;
import com.changgou.goods.pojo.Spec;
import com.changgou.goods.service.SpecService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
@Service
public class SpecServiceImpl implements SpecService {
    @Resource
    private SpecMapper specMapper;

    @Override
    public int add(Spec spec) {
        return specMapper.insertSelective(spec);
    }

    @Override
    public int delete(Long id) {
        return specMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Long id, Spec spec) {
        spec.setId(id);
        return specMapper.updateByPrimaryKeySelective(spec);
    }

    @Override
    public int update(Spec spec) {
        return specMapper.updateByPrimaryKeySelective(spec);
    }

    @Override
    public Spec findById(Long id) {
        return specMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Spec> findByCategoryId(Long categoryId) {
        return specMapper.findByCategoryId(categoryId);
    }

    @Override
    public List<Spec> findList(Spec spec) {
        Example example = buildExample(spec);
        return specMapper.selectByExample(example);
    }

    @Override
    public List<Spec> findAll() {
        return specMapper.selectAll();
    }

    @Override
    public PageInfo<Spec> page(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<Spec> albums = specMapper.selectAll();
        return new PageInfo<>(albums);
    }

    @Override
    public PageInfo<Spec> page(Integer page, Integer size, Spec spec) {
        PageHelper.startPage(page, size);
        Example example = buildExample(spec);
        List<Spec> albums = specMapper.selectByExample(example);
        return new PageInfo<>(albums);
    }

    private Example buildExample(Spec spec) {
        Example example = new Example(Spec.class);
        Example.Criteria criteria = example.createCriteria();
        if (spec != null) {
            if (spec.getId() != null) {
                criteria.andEqualTo("id", spec.getId());
            }
            if (StringUtils.isNotBlank(spec.getName())) {
                criteria.andLike("name", "%" + spec.getName() + "%");
            }
        }
        return example;
    }

}
