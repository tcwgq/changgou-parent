package com.changgou.goods.service;

import com.changgou.goods.pojo.Sku;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface SkuService {
    void add(Sku sku);

    void delete(Long id);

    void update(Sku sku);

    /**
     * 减库存
     *
     * @param map
     */
    void decrease(Map<Long, Integer> map);

    void increase(Map<Long, Integer> map);

    Sku findById(Long id);

    List<Sku> findAll();

    List<Sku> findList(Sku sku);

    PageInfo<Sku> findPage(int page, int size);

    PageInfo<Sku> findPage(Sku sku, int page, int size);

}
