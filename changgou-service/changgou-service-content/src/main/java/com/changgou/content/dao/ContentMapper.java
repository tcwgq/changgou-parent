package com.changgou.content.dao;

import com.changgou.content.pojo.Content;
import tk.mybatis.mapper.common.Mapper;

/**
 * ContentDao
 *
 * @author tcwgq
 * @since 2022/05/29 20:00
 */
public interface ContentMapper extends Mapper<Content> {

}
