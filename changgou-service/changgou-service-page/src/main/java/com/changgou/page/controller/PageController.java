package com.changgou.page.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.page.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
@RestController
@RequestMapping("/page")
public class PageController {
    @Autowired
    private PageService pageService;

    /**
     * 生成静态页面
     *
     * @param id SPU的ID
     */
    @GetMapping("/createHtml/{id}")
    public Result<String> createHtml(@PathVariable(name = "id") Long id) {
        pageService.createPageHtml(id);
        return new Result<>(true, StatusCode.OK, "ok");
    }

}
