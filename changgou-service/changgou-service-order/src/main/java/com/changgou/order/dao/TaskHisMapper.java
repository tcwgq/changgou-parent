package com.changgou.order.dao;

import com.changgou.order.bean.TaskHis;
import tk.mybatis.mapper.common.Mapper;

/**
 * TaskHisDao
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
public interface TaskHisMapper extends Mapper<TaskHis> {

}
