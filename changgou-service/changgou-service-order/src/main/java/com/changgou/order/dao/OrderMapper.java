package com.changgou.order.dao;

import com.changgou.order.bean.Order;
import tk.mybatis.mapper.common.Mapper;

/**
 * OrderDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderMapper extends Mapper<Order> {

}
