package com.changgou.order.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.common_spring.utils.TokenDecoder;
import com.changgou.order.bean.OrderItem;
import com.changgou.order.service.CartService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/6/14 16:03
 */
@RestController
@RequestMapping("/cart")
public class CartController {
    @Resource
    private CartService cartService;

    @PostMapping("/add")
    public Result<String> add(@RequestParam("skuId") Long skuId, @RequestParam("num") Integer num) {
        String username = TokenDecoder.getUsername();
        cartService.add(skuId, num, username);
        return new Result<>(true, StatusCode.OK, "添加购物车成功");
    }

    @GetMapping("/list")
    public Result<List<OrderItem>> list() {
        String username = TokenDecoder.getUsername();
        List<OrderItem> orderItems = cartService.list(username);
        return new Result<>(true, StatusCode.OK, "success", orderItems);
    }

}
