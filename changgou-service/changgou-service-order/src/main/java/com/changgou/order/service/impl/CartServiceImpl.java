package com.changgou.order.service.impl;

import com.changgou.common.response.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.feign.SpuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.goods.pojo.Spu;
import com.changgou.order.bean.OrderItem;
import com.changgou.order.service.CartService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/6/14 16:04
 */
@Service
public class CartServiceImpl implements CartService, InitializingBean {
    @Resource
    private RedisTemplate<String, OrderItem> redisTemplate;

    @Resource
    private SkuFeign skuFeign;

    @Resource
    private SpuFeign spuFeign;

    @Override
    public void afterPropertiesSet() {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Long.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(OrderItem.class));
    }

    @Override
    public void add(Long skuId, Integer num, String username) {
        /*
         * 1查询redis中的数据
         * 2如果redis中已经有了，则追加数量，重新计算金额
         * 3如果没有，将商品添加到缓存
         */
        OrderItem orderItem = (OrderItem) redisTemplate.boundHashOps("Cart:" + username).get(skuId);
        if (orderItem != null) {
            // 存在，刷新购物车
            int total = orderItem.getNum() + num;
            if (total <= 0) {
                redisTemplate.boundHashOps("Cart:" + username).delete(skuId);
                Long size = redisTemplate.boundHashOps("Cart:" + username).size();
                if (size == null || size == 0) {
                    redisTemplate.delete("Cart:" + username);
                }
                return;
            }
            orderItem.setNum(total);
            orderItem.setMoney(orderItem.getNum() * orderItem.getPrice());
            orderItem.setPayMoney(orderItem.getNum() * orderItem.getPrice());
            redisTemplate.boundHashOps("Cart:" + username).put(skuId, orderItem);
        } else {
            // 1.查询sku
            Result<Sku> result = skuFeign.findById(skuId);
            Sku sku = result.getData();
            if (sku != null && num > 0) {
                // 2.根据sku的数据对象 获取 该SKU对应的SPU的数据
                Long spuId = sku.getSpuId();
                Result<Spu> spuResult = spuFeign.findById(spuId);
                Spu spu = spuResult.getData();
                // 3.将数据存储到 购物车对象(order_item)中
                orderItem = new OrderItem();
                orderItem.setSpuId(spu.getId());
                orderItem.setSkuId(skuId);
                orderItem.setCategoryId1(spu.getCategory1Id());
                orderItem.setCategoryId2(spu.getCategory2Id());
                orderItem.setCategoryId3(spu.getCategory3Id());
                orderItem.setName(sku.getName());// 商品的名称 sku的名称
                orderItem.setPrice(sku.getPrice());// sku的单价
                orderItem.setNum(num);// 购买的数量
                orderItem.setMoney(orderItem.getNum() * orderItem.getPrice());// 单价* 数量
                orderItem.setPayMoney(orderItem.getNum() * orderItem.getPrice());// 单价* 数量
                orderItem.setImage(sku.getImage());// 商品的图片
                // 4.数据添加到redis中  key:用户名 field:sku的ID  value:购物车数据(order_item)
                redisTemplate.boundHashOps("Cart:" + username).put(skuId, orderItem);// hset key field value hget key field
            }
        }
    }

    @Override
    public List<OrderItem> list(String username) {
        // 注意，添加时设置了序列化器，查询时也要设置
        BoundHashOperations<String, Long, OrderItem> boundHashOperations = redisTemplate.boundHashOps("Cart:" + username);
        return boundHashOperations.values();
    }

    @Override
    public List<OrderItem> filter(String username, List<Long> ids) {
        BoundHashOperations<String, Long, OrderItem> boundHashOperations = redisTemplate.boundHashOps("Cart:" + username);
        return boundHashOperations.multiGet(ids);
    }

    @Override
    public void remove(String username, List<Long> ids) {
        redisTemplate.boundHashOps("Cart:" + username).delete(ids.toArray());
        Long size = redisTemplate.boundHashOps("Cart:" + username).size();
        if (size == null || size <= 0) {
            redisTemplate.delete("Cart:" + username);
        }
    }

}
