package com.changgou.order.service;

import com.changgou.order.bean.Order;
import com.github.pagehelper.PageInfo;

import java.util.Date;
import java.util.List;

/**
 * OrderService
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderService {
    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    Order create(Order order);

    /**
     * 确认收货
     *
     * @param orderId
     * @param operator
     */
    void take(String orderId, String operator);

    /**
     * 批量发货
     *
     * @param orders
     */
    void batchSend(List<Order> orders);

    /**
     * 15天未点击确认收货自动确认收货
     */
    void autoTack();

    /**
     * 新增Order
     *
     * @param order
     */
    void add(Order order);

    /**
     * 删除Order
     *
     * @param id
     */
    void delete(String id);

    void delete(String out_trade_no, String transaction_id, Date time_end);

    /**
     * 修改Order数据
     *
     * @param order
     */
    void update(Order order);

    void update(String out_trade_no, String transaction_id, Date time_end);

    /**
     * 30分钟未支付，自动关单
     *
     * @param orderId 订单ID
     */
    void close(String orderId);

    /**
     * 根据ID查询Order
     *
     * @param id
     * @return
     */
    Order findById(String id);

    /**
     * 查询所有Order
     *
     * @return
     */
    List<Order> findAll();

    /**
     * Order多条件搜索方法
     *
     * @param order
     * @return
     */
    List<Order> findList(Order order);

    /**
     * Order分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<Order> findPage(int page, int size);

    /**
     * Order多条件分页查询
     *
     * @param order
     * @param page
     * @param size
     * @return
     */
    PageInfo<Order> findPage(Order order, int page, int size);

}
