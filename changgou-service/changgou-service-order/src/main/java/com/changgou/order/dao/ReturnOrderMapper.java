package com.changgou.order.dao;

import com.changgou.order.bean.ReturnOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * ReturnOrderDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface ReturnOrderMapper extends Mapper<ReturnOrder> {

}
