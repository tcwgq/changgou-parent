package com.changgou.order.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.order.bean.Task;
import com.changgou.order.config.RabbitMQAddUserPointConfig;
import com.changgou.order.service.TaskService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DelTaskListener {
    @Autowired
    private TaskService taskService;

    @RabbitListener(queues = RabbitMQAddUserPointConfig.QU_BUYING_FINISH_ADD_POINT)
    public void receiveMessage(String message) {
        Task task = JSON.parseObject(message, Task.class);
        taskService.delTask(task);
    }

}