package com.changgou.order.listener;

import com.changgou.order.config.RabbitMQOrderAutoTackConfig;
import com.changgou.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * TODO XXL-Job实现
 */
@Slf4j
@Component
public class OrderAutoTackListener {
    @Resource
    private OrderService orderService;

    /**
     * TODO 消息幂等处理
     *
     * @param orderId 消息
     */
    @RabbitListener(queues = RabbitMQOrderAutoTackConfig.ORDER_PAY_NOTIFY_QUEUE)
    public void receiveMessage(String orderId) {
        log.info("orderId: {}", orderId);
        orderService.autoTack();
    }

}