package com.changgou.order.dao;

import com.changgou.order.bean.OrderConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * OrderConfigDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderConfigMapper extends Mapper<OrderConfig> {

}
