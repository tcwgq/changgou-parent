package com.changgou.canal.feign;

import com.changgou.common.response.Result;
import com.changgou.content.feign.ContentFeign;
import com.changgou.content.pojo.Content;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/6/8 17:30
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ContentFeignTest {
    @Autowired
    private ContentFeign contentFeign;

    @Test
    public void add() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void update() {
    }

    @Test
    public void findById() {
    }

    @Test
    public void findByCategory() {
        Result<List<Content>> result = contentFeign.findByCategory(1L);
        System.out.println(result.getData());
    }

    @Test
    public void findAll() {
    }

    @Test
    public void findList() {
    }

    @Test
    public void findPage() {
    }

    @Test
    public void testFindPage() {
    }
}