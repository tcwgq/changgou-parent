package com.changgou.user.config;

import com.changgou.order.bean.Task;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author tcwgq
 * @since 2022/6/25 11:02
 */
@Configuration
public class RedisTemplateConfig {
    @Bean("taskRedisTemplate")
    public RedisTemplate<String, Task> goodsRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Task> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Task.class));
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Long.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Task.class));
        return redisTemplate;
    }

}
