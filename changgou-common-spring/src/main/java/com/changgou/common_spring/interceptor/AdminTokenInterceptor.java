package com.changgou.common_spring.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author tcwgq
 * @since 2022/6/14 18:06
 */
public class AdminTokenInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        // 将原始请求头信息原样传到调用的微服务
        /*
         * TODO
         * 开启feign熔断，默认是线程池隔离时，requestAttributes为null，需要配置为信号量隔离
         */
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            // 1.获取请求对象
            HttpServletRequest request = requestAttributes.getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                // 2.获取请求对象中的所有的头信息(网关传递过来的)
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();// 头的名称
                    String value = request.getHeader(name);// 头名称对应的值
                    // 3.将头信息传递给feign (restTemplate)
                    template.header(name, value);
                }
            }
        }
    }

}
